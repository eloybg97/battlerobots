/*
 * Copyright (c) 2001-2023 Mathew A. Nelson and Robocode contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * https://robocode.sourceforge.io/license/epl-v10.html
 */
package myrobots;


import robocode.Robot;
import robocode.BulletHitEvent;
import robocode.BulletHitBulletEvent;
import robocode.BulletMissedEvent;
import robocode.DeathEvent;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.HitWallEvent;
import robocode.RobotDeathEvent;
import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import robocode.StatusEvent;
import robocode.WinEvent;
import robocode.Rules;

import robocode.util.Utils;


import java.awt.*;



public class MyRobot extends Robot {
	private double saveDistance;

    public MyRobot() {
		saveDistance = 50;
	}

	@Override
	public void run() {
		// Set colors
        //
        setAdjustRadarForGunTurn(true);
		//irRumbo(0);

        while(true) {
		        turnRadarLeft(360);
        }
	}

	@Override
	public void onBulletHit(BulletHitEvent e) {
		
	}

	@Override
	public void onBulletHitBullet(BulletHitBulletEvent e) {

	}

	@Override
	public void onBulletMissed(BulletMissedEvent e) {

	}

	@Override
	public void onDeath(DeathEvent e) {

	}

	@Override
    public void onHitByBullet(HitByBulletEvent e) {

	}

	@Override
	public void onHitRobot(HitRobotEvent e) {

	}

	@Override
    public void onHitWall(HitWallEvent e) {

	}

	@Override
	public void onRobotDeath(RobotDeathEvent e) {

	}

	@Override
	public void onRoundEnded(RoundEndedEvent e) {

	}

	@Override
	public void onScannedRobot(ScannedRobotEvent e) {
		double x = 0;
		double y = 0;

		toGunRelative(e.getBearing());
		fire(1);
		x = getEnemyPositionX(e.getDistance(), e.getBearing());
		y = getEnemyPositionY(e.getDistance(), e.getBearing());

		irPosicion(x, y);
		
	}

	@Override
	public void onStatus(StatusEvent e) {

	}

	@Override
	public void onWin(WinEvent e) {

	}

	//Programada por mi. Va al centro del campo de batalla	//Programada por mi. Va a la posicion X,Y del campo de batalla
	private void irPosicion(double x, double y) {
		double myX = getX();
		double myY = getY();
		double dist = 0;
		double anguloRotacion = 0;
		double anguloCorrectivo = (myY < y)?0:180;

		dist = Math.sqrt((x - myX) * (x - myX) + (y - myY) * (y - myY));

		anguloRotacion = Math.atan((x - myX) / (y - myY));
		anguloRotacion = Math.toDegrees(anguloRotacion) + anguloCorrectivo - getHeading();
	
		anguloRotacion = Utils.normalRelativeAngleDegrees(anguloRotacion);
		turnRight(anguloRotacion);
		ahead(dist);
	}

	private void irRumbo(double degree) {
		double anguloRotacion = -getHeading() + degree;
		anguloRotacion = Utils.normalRelativeAngleDegrees(anguloRotacion);

		turnRight(anguloRotacion);
	}

	private void toGun(double degree) {
		double anguloRotacion = 0;
		anguloRotacion = -getGunHeading() + degree;
		anguloRotacion = Utils.normalRelativeAngleDegrees(anguloRotacion);

		turnGunRight(anguloRotacion);
	}

	private void toGunRelative(double degree) {
		double anguloRotacion = 0;
		anguloRotacion = -getGunHeading() + getHeading() + degree;
		anguloRotacion = Utils.normalRelativeAngleDegrees(anguloRotacion);

		turnGunRight(anguloRotacion);
	}

	private double getEnemyPositionX(double enemyDistance, double enemyDegree) {
		enemyDegree = Math.toRadians(enemyDegree + getHeading());
		return getX() + (enemyDistance - saveDistance) * Math.sin(enemyDegree);
	}

	private double getEnemyPositionY(double enemyDistance, double enemyDegree) {
		enemyDegree = Math.toRadians(enemyDegree + getHeading());
		return getY() + (enemyDistance - saveDistance) * Math.cos(enemyDegree);
	}

}
